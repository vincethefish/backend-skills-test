import * as fs from "fs-extra";

export function parseBulkPalindromeFile(filePath: string) {
  let data = fs.readFileSync(filePath, { encoding: "utf-8" });
  let dataArray: string[] = data.split("\r\n");
  let responseObj: { [key: string]: boolean } = {};
  for (let i of dataArray) {
    responseObj[i] = isPalindrome(i);
  }
  return responseObj;
}

export function isPalindrome(inputString: string) {
  const stringToTest = inputString.toLowerCase();
  if (stringToTest.length === 0) {
    return false;
  } else if (stringToTest.length === 1) {
    return true;
  }
  let compareString: string = "";
  for (let i = stringToTest.length - 1; i >= 0; i--) {
    compareString = compareString + stringToTest[i];
  }
  if (compareString == stringToTest) {
    return true;
  } else {
    return false;
  }
}
