import * as express from "express";
import { parseBulkPalindromeFile, isPalindrome } from "../actions";
import { BULK_PALINDROME_FILE } from "../constants";

export class Palindrome {
  constructor() {}

  public routes(api: express.Application): void {
    api
      .route("/palindrome/bulk")
      .get((req: express.Request, res: express.Response) => {
        let responseObj = parseBulkPalindromeFile(BULK_PALINDROME_FILE);
        res.status(200).send(responseObj);
      });
    api
      .route("/palindrome")
      .post((req: express.Request, res: express.Response) => {
        let inputString: any = req.query.input;
        let result = isPalindrome(inputString);
        res.send({ result: result });
      });
  }
}
