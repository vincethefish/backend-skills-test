import express from "express";
import { Palindrome } from "../routes/routes";
class Api {
  public api: express.Application;
  public palindromeRoutes = new Palindrome();
  constructor() {
    this.api = express();
    this.config();
    this.palindromeRoutes.routes(this.api);
  }

  private config(): void {}
}

export default new Api().api;
